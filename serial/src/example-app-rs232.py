import logging
import redis
import serial_frames_pb2

logging.basicConfig(filename="/var/log/example-app-rs232.log", level=logging.INFO, format="%(asctime)s [%(levelname)-8s] %(message)s")
logging.info('Starting application...')

# Create instance to local Redis
r = redis.Redis(host='127.0.0.1', port=6379)

# Connect to Redis PubSub
p = r.pubsub()
logging.info('Connected to Redis.')

# Subscribe to default channel for reading RS232 data
p.subscribe('/vcu/apps/serialrx@rs232/read/v1')

# Listen to messages
for message in p.listen():
    data = message['data']
    # Just check for the bytes messages as other message may appear on the Redis channel
    if isinstance(data, bytes):
        serialFrames = serial_frames_pb2.SerialFrames()
        serialFrames.ParseFromString(data)
        for serialFrame in serialFrames.frames:
            print('Serial data: ' + serialFrame.data.hex())