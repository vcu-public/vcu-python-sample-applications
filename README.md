# VCU Sample Applications
This project contains samples, demos and tutorials for developing applications on the ZF VCU platform.
By default the VCU software platform provides a set of system components and services that can be used to integrate applications on different layers.
Moreover, it supports Python, Java and native C++ for building and running custom applications.

The following tutorials and code snippets make use of the common data/inter-process exchange layer built upon Redis.
Data from peripherals like CAN, GNSS, GPIOs is published to dedicated Redis channels which can be subscribed by applications easily.
Messages in Redis channels are usually encoded with Protobuf.

This project outlines how to work with Python and the VCU's peripheral data that is exposed via dedicated Redis channels.
The code examples are written in Python 3, the Protobuf definitions for the various data messages are already prepared as Python classes.
The generation of Python classes representing the Protobuf definitions can be done using Google's Protobuf compiler.

## CAN
This example illustrates how to read CAN frames from the CAN1 interface.
An application just needs to subscribe to Redis channel `/vcu/apps/canrx@can0/can/v1` and listen to incoming messages.

```python
r = redis.Redis(host='127.0.0.1', port=6379)
p = r.pubsub()
p.subscribe('/vcu/apps/canrx@can0/can/v1')

# Listen to messages
for message in p.listen():
    data = message['data']
    # Just check for the bytes messages as other message may appear on the Redis channel
    if isinstance(data, bytes):
        canFrames = can_frames_pb2.CanFrames()
        canFrames.ParseFromString(data)                
        for canFrame in canFrames.frames:
            print('CAN id=' + str(hex(canFrame.id)) + ', data=' + canFrame.data.hex())
```

## GNSS
This example illustrates how to access position information from the GNSS module.
Location information is typically contained in TPV messages following the [NMEA](https://en.wikipedia.org/wiki/NMEA_0183) standard and provided by the system's gpsd service.
Therefore an application simply needs to subscribe and listen to Redis channel `/vcu/apps/gpsdrx@gps0/tpv/v1`.

```python
r = redis.Redis(host='127.0.0.1', port=6379)
p = r.pubsub()
p.subscribe('/vcu/apps/gpsdrx@gps0/tpv/v1')

# Listen to messages
for message in p.listen():
    data = message['data']
    # Just check for the bytes messages as other message may appear on the Redis channel
    if isinstance(data, bytes):
        gpsdFrame = gps_frames_pb2.GPSMessage()
        gpsdFrame.ParseFromString(data)
        location = ''
        for gpsdField in gpsdFrame.tpv:
            if gpsdField.key == 'lat':
                location += 'lat='+str(gpsField.doubleVal)
            if gpsdField.key == 'lon':
                location += ', lon='+str(gpsField.doubleVal)
            if gpsdField.key == 'alt':
                location += ', alt='+str(gpsField.doubleVal)
        
        print('Location: ', location)         
```

## IMU
This example illustrates how to access acceleration and gyroscope values from the VCU's integrated IMU.
To work with IMU data, an application may just subscribe and listen to Redis channel `/vcu/apps/gpsdrx@imu0/att/v1`.

```python
r = redis.Redis(host='127.0.0.1', port=6379)
p = r.pubsub()
p.subscribe('/vcu/apps/gpsdrx@imu0/att/v1')

# Listen to messages
for message in p.listen():
    data = message['data']
    # Just check for the bytes messages as other message may appear on the Redis channel
    if isinstance(data, bytes):
        gpsdFrame = gps_frames_pb2.GPSMessage()
        gpsdFrame.ParseFromString(data)
        imuText = ''
        for gpsdField in gpsdFrame.att:
            if gpsdField.key == 'accx':
                imuText += 'acc x='+str(gpsdField.doubleVal)
            if gpsdField.key == 'accy':
                imuText += ', acc y='+str(gpsdField.doubleVal)
            if gpsdField.key == 'accz':
                imuText += ', acc z='+str(gpsdField.doubleVal)
            if gpsdField.key == 'gyrox':
                imuText += ', gyro x='+str(gpsdField.doubleVal)
            if gpsdField.key == 'gyroy':
                imuText += ', gyro y='+str(gpsdField.doubleVal)                
            if gpsdField.key == 'pitch':
                imuText += ', pitch='+str(gpsdField.doubleVal)  
            if gpsdField.key == 'yaw':
                imuText += ', yaw='+str(gpsdField.doubleVal)  
            if gpsdField.key == 'roll':
                imuText += ', roll='+str(gpsdField.doubleVal)                
        print('IMU: ', imuText)
```

## GPIO
This example shows how to read digital inputs and control outputs on the VCU.
By default, the VCU features 4 inputs and 4 outputs. The state of inputs can be read from Redis channel `/vcu/apps/gpiorx@gpio0/gpi/v1`, while outputs can be controlled by publishing their state to channel `/vcu/apps/gpiorx@gpio0/gpo/v1`.

**GPIs:**
```python
r = redis.Redis(host='127.0.0.1', port=6379)
p = r.pubsub()
p.subscribe('/vcu/apps/gpiorx@gpio0/gpi/v1')

# Listen to messages
for message in p.listen():
    data = message['data']
    # Just check for the bytes messages as other message may appear on the Redis channel
    if isinstance(data, bytes):
        gpiFrames = gpio_frames_pb2.GpiFrames()
        gpiFrames.ParseFromString(data)
        for gpiFrame in gpiFrames.frames:
          timestamp = gpiFrame.timedelta + gpiFrames.timestamp
          print('GPI({}) - Raw({}) - Val({}) - TimeStamp({})'.format(gpiFrame.channel, gpiFrame.rawValue, gpiFrame.value, timestamp))
        print('--')
```

**GPOs:**
```python
r = redis.Redis(host='127.0.0.1', port=6379)
channel = '/vcu/apps/gpiorx@gpio0/gpo/v1'

while True:
  gpoFrame = gpio_frames_pb2.GpoFrame()
  gpoFrame.timestamp = int(time.time() * 1000)

  # Ask for the output channel and its value on the console
  gpoFrame.channel = int(input('output channel: '))
  gpoFrame.value = int(input('output value: '))

  print('--')

  # Publish the output message
  r.publish(channel, gpoFrame.SerializeToString())
```

## Serial
This example shows how to exchange data with the VCU's RS232 interface.
For reading data from the RS232 serial interface, an application needs to listen on Redis channel `/vcu/apps/serialrx@rs232/read/v1`. 
Writing data to the serial interface can be done by just publishing messages to Redis channel `/vcu/apps/serialrx@rs232/write/v1`. 

```python
r = redis.Redis(host='127.0.0.1', port=6379)
p = r.pubsub()
p.subscribe('/vcu/apps/serialrx@rs232/read/v1')

# Listen to messages
for message in p.listen():
    data = message['data']
    # Just check for the bytes messages as other message may appear on the Redis channel
    if isinstance(data, bytes):
        serialFrames = serial_frames_pb2.SerialFrames()
        serialFrames.ParseFromString(data)
        for serialFrame in serialFrames.frames:
            print('Serial data: ' + serialFrame.data.hex())
```

## K-Line
Coming soon...

## LIN
Coming soon...
<br>
<br>
<br>
***
<small>(C) 2020 [ZF Friedrichshafen AG](https://www.zf.com)</small>