import logging
import time
import redis
import gpio_frames_pb2

logging.basicConfig(filename="/var/log/example-app-gpo.log", level=logging.INFO, format="%(asctime)s [%(levelname)-8s] %(message)s")
logging.info('Starting application...')

# Create instance to local Redis
r = redis.Redis(host='127.0.0.1', port=6379)

# Define the default GPIO channel for publishing output messages
channel = '/vcu/apps/gpiorx@gpio0/gpo/v1'

while True:
  gpoFrame = gpio_frames_pb2.GpoFrame()
  gpoFrame.timestamp = int(time.time() * 1000)

  # Ask for the output channel and value on the console
  gpoFrame.channel = int(input('output channel: '))
  gpoFrame.value = int(input('output value: '))

  print('--')

  # Publish the output message
  r.publish(channel, gpoFrame.SerializeToString())
  logging.info('Published output message on ' + channel)