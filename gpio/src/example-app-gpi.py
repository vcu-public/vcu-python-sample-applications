import logging
import redis
import gpio_frames_pb2

logging.basicConfig(filename="/var/log/example-app-gpi.log", level=logging.INFO, format="%(asctime)s [%(levelname)-8s] %(message)s")
logging.info('Starting application...')

# Create instance to local Redis
r = redis.Redis(host='127.0.0.1', port=6379)

# Connect to Redis PubSub
p = r.pubsub()
logging.info('Connected to Redis.')

# Subscribe to default GPIO channel with input messages
p.subscribe('/vcu/apps/gpiorx@gpio0/gpi/v1')

# Listen to messages
for message in p.listen():
    data = message['data']
    # Just check for the bytes messages as other message may appear on the Redis channel
    if isinstance(data, bytes):
        gpiFrames = gpio_frames_pb2.GpiFrames()
        gpiFrames.ParseFromString(data)

        for gpiFrame in gpiFrames.frames:
          timestamp = gpiFrame.timedelta + gpiFrames.timestamp
          print('GPI({}) - Raw({}) - Val({}) - TimeStamp({})'.format(gpiFrame.channel, gpiFrame.rawValue, gpiFrame.value, timestamp))

        print('--')