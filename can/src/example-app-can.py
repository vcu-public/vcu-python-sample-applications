import logging
import redis
import can_frames_pb2

logging.basicConfig(filename="/var/log/example-app-can.log", level=logging.INFO, format="%(asctime)s [%(levelname)-8s] %(message)s")
logging.info('Starting application...')

# Create instance to local Redis
r = redis.Redis(host='127.0.0.1', port=6379)

# Connect to Redis PubSub
p = r.pubsub()
logging.info('Connected to Redis.')

# Subscribe to default channel for CAN0
p.subscribe('/vcu/apps/canrx@can0/can/v1')

# Listen to messages
for message in p.listen():
    data = message['data']
    # Just check for the bytes messages as other message may appear on the Redis channel
    if isinstance(data, bytes):
        canFrames = can_frames_pb2.CanFrames()
        canFrames.ParseFromString(data)
                
        for canFrame in canFrames.frames:
            print('CAN id=' + str(hex(canFrame.id)) + ', data=' + canFrame.data.hex())