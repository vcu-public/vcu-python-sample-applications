import logging
import redis
import gps_frames_pb2

logging.basicConfig(filename="/var/log/example-app-gps.log", level=logging.INFO, format="%(asctime)s [%(levelname)-8s] %(message)s")
logging.info('Starting application...')

# Create instance to local Redis
r = redis.Redis(host='127.0.0.1', port=6379)

# Connect to Redis PubSub
p = r.pubsub()
logging.info('Connected to Redis.')

# Subscribe to default GPS channel with TPV messages
p.subscribe('/vcu/apps/gpsdrx@gps0/tpv/v1')

# Listen to messages
for message in p.listen():
    data = message['data']
    # Just check for the bytes messages as other message may appear on the Redis channel
    if isinstance(data, bytes):
        gpsdFrame = gps_frames_pb2.GPSMessage()
        gpsdFrame.ParseFromString(data)
        gpsText = ''
        for gpsdField in gpsdFrame.tpv:
            if gpsdField.key == 'lat':
                gpsText += 'lat='+str(gpsdField.doubleVal)
            if gpsdField.key == 'lon':
                gpsText += ', lon='+str(gpsdField.doubleVal)
            if gpsdField.key == 'alt':
                gpsText += ', alt='+str(gpsdField.doubleVal)
        
        print('Location: ', gpsText)