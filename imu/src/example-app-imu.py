import logging
import redis
import gps_frames_pb2

logging.basicConfig(filename="/var/log/example-app-imu.log", level=logging.INFO, format="%(asctime)s [%(levelname)-8s] %(message)s")
logging.info('Starting application...')

# Create instance to local Redis
r = redis.Redis(host='127.0.0.1', port=6379)

# Connect to Redis PubSub
p = r.pubsub()
logging.info('Connected to Redis.')

# Subscribe to default IMU channel with ATT messages
p.subscribe('/vcu/apps/gpsdrx@imu0/att/v1')

# Listen to messages
for message in p.listen():
    data = message['data']
    # Just check for the bytes messages as other message may appear on the Redis channel
    if isinstance(data, bytes):
        gpsdFrame = gps_frames_pb2.GPSMessage()
        gpsdFrame.ParseFromString(data)
        imuText = ''
        for gpsdField in gpsdFrame.att:
            if gpsdField.key == 'accx':
                imuText += 'acc x='+str(gpsdField.doubleVal)
            if gpsdField.key == 'accy':
                imuText += ', acc y='+str(gpsdField.doubleVal)
            if gpsdField.key == 'accz':
                imuText += ', acc z='+str(gpsdField.doubleVal)
            if gpsdField.key == 'gyrox':
                imuText += ', gyro x='+str(gpsdField.doubleVal)
            if gpsdField.key == 'gyroy':
                imuText += ', gyro y='+str(gpsdField.doubleVal)                
            if gpsdField.key == 'pitch':
                imuText += ', pitch='+str(gpsdField.doubleVal)  
            if gpsdField.key == 'yaw':
                imuText += ', yaw='+str(gpsdField.doubleVal)  
            if gpsdField.key == 'roll':
                imuText += ', roll='+str(gpsdField.doubleVal)        
        
        print('IMU: ', imuText)